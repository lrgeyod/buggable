import { StatusBar } from 'expo-status-bar';
import { Button, StyleSheet, Text, View } from 'react-native';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import YodController from 'lrge_yod_rn';
import { useState } from 'react';


export default function App() {

  const [last, setlast] = useState();

  const yodController = new YodController("TOKEN", "121.017.347-61", "testeSMT");

  function start(){
    yodController.start();
  }

  return (
    <View style={styles.container}>
      <Text>Antes</Text>
      <MapView
        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
        style={styles.map}
        initialRegion={{
          latitude: -21.7772717,
          longitude: -41.3846027,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
      />
      <Button
      title='START'
      onPress={start}>
        START
      </Button>
      <Button
      title='LAST PAYLOAD'
      onPress={async ()=>{
        setlast(await yodController.getLastPayload() );
      }}>
      </Button>
      <Button
      title='SEND'
      onPress={()=>{
        yodController.stop();
      }}>
      </Button>
      <Text>
        {JSON.stringify(last)}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
